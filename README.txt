This is a copy/fork if the field_permissions module, making it work per
instance, instead of per field. Most of the code is directly taken from the
field_permissions module.

There are probably still things that need to be cleaned up, and I am not sure
if this will work reliably. Concerns are mainly with the performance impact.
The other problem is that there are situations in which it is not possible to
discern the field_instance in a hook_field_permissions(). Please test if your
use-case is handled, and report findings. For production it is best to stick to
the original field_permissions module, which is mature, reliable and tested.

Currently the implementation is made so it doesn't rely or interfere with
the field_permissions module, so both modules can be put in the same project,
or can even be in use on the same field. This is not tested very well yet.

Thanks to markus_petrux and RobLoach for creating the excellent
field_permissions module.
